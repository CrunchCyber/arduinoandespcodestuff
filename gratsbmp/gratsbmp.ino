#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bmp; // I2C

const char* ssid     = "";
const char* password = ""; //leave empty for no password
String stationID = String("station01"); //change to set station id
const char* SERVER = ""; //server url
int meassurmentInterval = 60000*3; // set to 3min interal

void setup() {

 
  Serial.begin(115200);
  delay(10);


  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }
}

void loop() {

  String temperature = String(bmp.readTemperature()* 1.8000 + 32.00);


  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    http.begin(SERVER);
    http.addHeader("Content-Type", "application/json");

    int httpCode = http.POST("{\"stationID\":" + stationID + ",\"temperature\":" + temperature + "}");

    if (httpCode > 0) { //Check the returning code

      String payload = http.getString();   //Get the request response payload
      Serial.println(payload);                     //Print the response payload
     Serial.println(httpCode);
    } else {
      Serial.println(httpCode);
    }
    
    http.end();

  } else {
    Serial.println("problem");
  }

  ESP.deepSleep(meassurmentInterval);
}
