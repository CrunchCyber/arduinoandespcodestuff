#include <MIDI.h>
#include <Arduino.h>
#include <A4988.h>

#define MOTOR_STEPS 200
#define RPM 120
#define MICROSTEPS 1
//interrupt pin for homing routine
const byte interruptPin = 2;
//stepper motor pins
const byte stepPin = 3;
const byte directionPin = 4;
//Pins for micro stepping
const byte ms1 = 7;
const byte ms2 = 6;
const byte ms3 = 5;
const byte sleep = 13;
//pins for laser
const byte laser = 8;
const byte played = A0;
volatile bool stopSig = false;

A4988 stepper(MOTOR_STEPS, directionPin, stepPin, sleep, ms1, ms2, ms3);
MIDI_CREATE_DEFAULT_INSTANCE();

void setup() {
  //Setup pins
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(laser, OUTPUT);
  pinMode(played, INPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin), endHoming, FALLING);
  
  //Begin Midi before Serial, otherwise it will lead to problems
  MIDI.begin();
  //Begin Serial
  //TODO add check if midi or midi via serial is used
  Serial.begin(115200); 
  delay(10);

  //Begin homing of the mirror and later move it to start position
  stepper.begin(1, MICROSTEPS);
  while(!stopSig){
    stepper.rotate(-10);
    delay(100);
  }
  stepper.setRPM(160);
  stepper.move(35);
}

void loop() {
  //TODO everything, this is for now a test to see if i can send midi via serial while the stepper is moving. and it works
  //next step will be wiring up the laser without breaking it and adjust stepper motor distance per note
  stepper.move(-15);
  stepper.move(15);
  MIDI.sendNoteOn(42, 127, 1);
}

//Interrupt Function to end Homing
void endHoming() {
  stopSig = true;
  digitalWrite(LED_BUILTIN, HIGH);
  detachInterrupt(digitalPinToInterrupt(interruptPin));
}
