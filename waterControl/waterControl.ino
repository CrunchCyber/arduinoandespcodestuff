#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

//webpage contents in program memory
const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<head>
<style>
a {
  width: 40%;
    height: 25px;
    padding: 10px;
    margin: 5px;
    text-align: center;
    border-radius: 5px;
    color: white;
    font-weight: bold;
    display: block;
    float: left;
}
.button {
    background: #4E9CAF;
}
.button-master {
    background: #c00;
}
</style>
<title>Water Control</title>
</head>
<body>

<a class="button-master" href="waterOff?valve=0">MASTER OFF</a>
<a class="button-master" href="waterOn?valve=0">MASTER ON</a>
<br>
<a class="button" href="waterOn?valve=1">WATER 1 ON</a>
<a class="button" href="waterOff?valve=1">WATER 1 OFF</a><br>
<a class="button" href="waterOn?valve=2">WATER 2 ON</a>
<a class="button" href="waterOff?valve=2">WATER 2 OFF</a><br>
<a class="button" href="waterOn?valve=3">WATER 3 ON</a>
<a class="button" href="waterOff?valve=3">WATER 3 OFF</a><br>
<a class="button" href="waterOn?valve=4">WATER 4 ON</a>
<a class="button" href="waterOff?valve=4">WATER 4 OFF</a><br>



</body>
</html>
)=====";

const char WATER_page[] PROGMEM = R"=====(

-1
)=====";

int valves[] = {14, 2, 0, 4, 5};
#define SENSORPIN 13
#define SENSORINTERUPTPIN 13

//SSID and Password of your WiFi router
const char* ssid = "dork";
const char* password = "";

float calibrationFactor = 4.5;

volatile byte pulseCount;  

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;
unsigned long waterUsed;
unsigned long oldTime;


ESP8266WebServer server(80); 

void setup(void){
  Serial.begin(115200);
  
  WiFi.begin(ssid, password); 
  Serial.println("");

  //Onboard WATER port Direction output
  for(int i = 0; i < sizeof(valves); i++){
    pinMode(valves[i], OUTPUT);
    if(i >= 1){
      digitalWrite(valves[i],HIGH);
    }
  }
  digitalWrite(valves[0], LOW);
  
  pinMode(SENSORPIN, INPUT);
  digitalWrite(SENSORPIN, HIGH);
  
  //Set variables
  pulseCount = 0;
  flowRate = 0.0;
  flowMilliLitres = 0;
  totalMilliLitres = 0;
  oldTime = 0;

  attachInterrupt(SENSORINTERUPTPIN, pulseCounter, FALLING);

  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  
 
  server.on("/", handleRoot);      //Which routine to handle at root location. This is display page
  server.on("/waterOn", handleWATERon); //as Per  <a href="WATEROn">, Subroutine to be calWATER
  server.on("/waterOff", handleWATERoff);
  server.on("/waterStats", handleWATERstats);
  server.on("/waterReset", handleWATERreset);
  
  server.begin();                  //Start server
  Serial.println("HTTP server started");
  
}

void loop(void){
  server.handleClient();          //Handle client requests
    
   if((millis() - oldTime) > 1000)    // Only process counters once per second
  { 
    detachInterrupt(SENSORINTERUPTPIN);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    waterUsed = totalMilliLitres/1000;
 
    pulseCount = 0;
   
    attachInterrupt(SENSORINTERUPTPIN, pulseCounter, FALLING);
  }  
}
//handle webpage stuff
void handleRoot() {
 String s = MAIN_page; //Read HTML contents
 server.send(200, "text/html", s); //Send web page
}

void handleWATERon() { 
 int valve = server.arg(0).toInt();
 digitalWrite(valves[valve],LOW);

 server.sendHeader("Location", String("/"), true);
 server.send ( 302, "text/plain", "");
}

void handleWATERoff() { 
 int valve = server.arg(0).toInt();
 digitalWrite(valves[valve],HIGH);

 server.sendHeader("Location", String("/"), true);
 server.send ( 302, "text/plain", "");
}

void handleWATERstats() {
  String s = String(waterUsed);
  server.send(200,"text/html", s);
}

void handleWATERreset(){
  waterUsed = 0;
  server.send(200);
}

//Interrupt routine
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
