#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme; // I2C

const char* ssid     = ""; //WIFI SSID
const char* password = ""; //WIFI Password

const char* SERVER = "http://192.168.1.105:3000/weather"; //Server where report should be sent to as a post request

int meassurmentInterval = 60000*10;

//Pins for analog/digital multiplexer, to overcome the 1 analog pin limitation of the ESP8266
int S0 = 14;
int S1 = 12;
int S2 = 13;
int S3 = 15;

void setup() {

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  
  
  Serial.begin(115200);
  delay(10);
 
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  bool status;
  status = bme.begin();
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
}

void loop() {
  
  //Read data from sensors and convert them to strings
  String temperature = String(bme.readTemperature());
  String humidity = String(bme.readHumidity());
  String pressure = String(bme.readPressure());
  String rain = String(readRain());
  String brightness = String(readBrightness());
  String windDirection = "none";
  String windStrength = String(-1);

  //Send report to server as a post request
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.begin(SERVER); 
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST("{\"stationID\": \"station01\", \"temperature\":" + temperature + ", \"humidity\":" + humidity + ", \"pressure\":" + pressure + ", \"rain\":" + rain + ", \"brightness\":" + brightness + ", \"windDirection\": \"none\", \"windStrength\": -1 }");
    if (httpCode > 0) { //Check the returning code
      String payload = http.getString();   //Get the request response payload
      Serial.println(payload);             //Print the response payload
    Serial.println(httpCode);
    } else {
      Serial.println(httpCode);
    }
    
    http.end();

  } else {
    Serial.println("Report could not be sent to the Server");
  }

  delay(meassurmentInterval);
}

int readBrightness() {
  digitalWrite(S0, LOW);
  digitalWrite(S1, LOW);
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);
  return analogRead(A0);
}

bool readRain() {
  digitalWrite(S0, LOW);
  digitalWrite(S1, HIGH);
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);
  int value = analogRead(A0);
  if (value > 0) {
    return false;
  } else {
    return true;
  }
}
